function guardarDatosUsuario() {
  var nombre = txtName.value;
  var email = txtEmail.value;
  var dni = txtDni.value;
  var usuario = {"nombre": nombre, "email": email, "dni": dni};
  //var usuario = {nombre ,email, dni}; FORMA CORTA CUANDO EL NOMBRE DE LA VARIABLE ES IGUAL AL DEL CAMPO.
  localStorage.setItem("nombre", nombre);
  sessionStorage.setItem("nombre", nombre);
  localStorage.setItem("usuarioLS", JSON.stringify(usuario));
  sessionStorage.setItem("usuarioSS", JSON.stringify(usuario));
}
function recuperarDatosUsuario() {
  var local = localStorage.getItem("nombre");
  var session = sessionStorage.getItem("nombre");
  var usuarioLocal = localStorage.getItem("usuarioLS");
  var usuarioSession = localStorage.getItem("usuarioSS");
  var resultLocal = JSON.parse(usuarioLocal);
  alert("Local Name: " + resultLocal.nombre);
  alert("Local: " + local + ", Session: " + session);
  alert("Usuario Local: " + usuarioLocal + ", Usuario Session: " + usuarioSession);
  document.getElementById("txtName").value = local;
}
